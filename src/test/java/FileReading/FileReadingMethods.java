package FileReading;

import org.apache.commons.io.FileUtils;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileReadingMethods {

    public void printFromUrl(String url) throws IOException {
        URL urlObject = new URL(url);
        URLConnection urlConnection = urlObject.openConnection();
        InputStream inputStream = urlConnection.getInputStream();
        String data = readFromInputStream(inputStream);
        System.out.println(data);
    }

    private String readFromInputStream(InputStream inputStream)
            throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br
                     = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }

    public static void main(String[] args) throws IOException {

        /*FileOutputStream fos = new FileOutputStream("file.txt");
        fos.write("hello\nmotor".getBytes());
        fos.close();
        fos.flush();
        */


        // 1st way to read and print
        FileInputStream fis = new FileInputStream("file.txt");
        Scanner scanner = new Scanner(fis);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            System.out.println(line);
        }

        // 2nd way to read and print
        try (Stream<String> stream = Files.lines(Paths.get("file.txt"))) {
            stream.forEach(System.out::println);
        }

        // 3nd way to read and print

        List<String> lines = FileUtils.readLines(new File("file.txt"), "UTF-8");
        for (String line : lines) {
            System.out.println(line);
        }

        // 4th way to read and print
        BufferedReader br = new BufferedReader(new FileReader("file.txt"));
        try {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        } finally {
            br.close();
        }
        // 5th way, similar to 4th way
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("file.txt"))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }
        }
        // 6th way to read and print
        List<String> list = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("file.txt"))) {
            list = bufferedReader.lines().collect(Collectors.toList());
            list.forEach(System.out::println);

        }
        // 7th way
        try (Stream<String> stream = Files.lines(Paths.get("file.txt"))) {
            stream
                    .filter(s -> s.contains("hello"))
//                    .sorted()
//                  .map(String::toUpperCase)
                    .forEach(System.out::println);
        }

    }

}
